import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
    request();
  }

  @override
  void initState() {
    request();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Future<Map<String, dynamic>> request() async {

    // todo - fix baseUrl
    var url = 'https://graph.facebook.com/v7.0/249000903204791/products?access_token=EAAIUKYEX0oIBACq3X7UjdPKZCiRCsQSpa6Hq0AY5mjqn3y2yhxxNMQfnxcSo4LJRug740hf8Upv6P9M3Sj6FOk3IcNZAHFZBHwrvRvwFcK15bWHlJDPtdyC37t6xk5yeUcTNqlvwXArHasfZCDs6ZBhhuVIvxrFZBkoSkPKZADqdMeplSv5qeZARqDD7f4vcxCQZD';
    var body = json.encode({
      "name": "Demo Cloth",
      "currency": "INR",
      "price": "12231",
      "category": "men_cloth",
      "retailer_id": "pages_commerce_sell5ed226862aa278",
      "image_url": "https://i.picsum.photos/id/163/300/300.jpg",
      "description": "Demo description is not Required",
      "url": "https://i.picsum.photos/id/163/300/300.jpg"
    });
//    curl -i -X GET \
//    "https://graph.facebook.com/v7.0/249000903204791/products?access_token=EAAIUKYEX0oIBADKZCJy5Jbed5lEZBaksKzV6DnB2gBhymYj8Y2QxezFz3Q3lmCU0pzM0tZC3S4PBQ5qmSZCF1ESG7gHFggEWZCXYCZBDLlB55IVqMDDWZBsjk8VlvMWXErVR9HkxyfzIp7sw9ehAm8Hb6PlhlcVNJWRED2IbX836Br0kQjv5unGFWaXamBI8WoZD"
    print('Body: $body');

    var response = await http.post(
      url,
      headers: {
        'accept': 'application/json',
      },
      body: {
        "name": "Demo Cloth",
        "currency": "INR",
        "price": "12231",
        "category": "men_cloth",
        "retailer_id": "pages_commerce_sell5ed226862aa278",
        "image_url": "https://i.picsum.photos/id/163/300/300.jpg",
        "description": "Demo description is not Required",
        "url": "https://i.picsum.photos/id/163/300/300.jpg"
      },
    );

    // todo - handle non-200 status code, etc
    print(response.body);
    return json.decode(response.body);
  }
}
